=======
Minimal
=======

A minimialistic watchface for Pebble. Colors can be configured in the Pebble app.

.. image:: light.png
.. image:: dark.png
.. image:: color.png
