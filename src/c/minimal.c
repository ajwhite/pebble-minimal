#include <pebble.h>

static Window *main_window;

static TextLayer *time_layer;
static TextLayer *date_layer;
static Layer     *battery_layer;

static int current_battery_tier;

static GColor foreground_color;
static GColor background_color;

enum {
    PS_FG_COLOR_KEY = 0,
    PS_BG_COLOR_KEY,
};

#define INBOX_SIZE 64
#define OUTBOX_SIZE 64

static void time_tick_handler(struct tm *time_tick, TimeUnits units_changed)
{
    static int old_date = -1;

    /* Update time layer */
    static char time_str[] = "01:00";
    strftime(time_str, sizeof(time_str), clock_is_24h_style() ? "%H:%M" : "%I:%M", time_tick);
    text_layer_set_text(time_layer, time_str);

    /* Update date layer */
    if (old_date == (time_tick->tm_mday + time_tick->tm_mon))
        return; /* No need to update the screen if there's no change */

    static char date_str[] = "Mon, Jan 01";
    strftime(date_str, sizeof(date_str), "%a, %b %d", time_tick);
    text_layer_set_text(date_layer, date_str);

    old_date = time_tick->tm_mday + time_tick->tm_mon; /* Store time as day + month for quick comparison */
}


static void update_battery(Layer *layer, GContext *ctx)
{
    /* Set starting point for circles */
    uint16_t x = PBL_IF_ROUND_ELSE(57, 39);
    uint16_t y = 4;

    /* Draw the circles */
    graphics_context_set_stroke_color(ctx, foreground_color);
    graphics_context_set_fill_color(ctx, foreground_color);

    for (int i = 0; i < 5; i++, x += 15) {
        if (i <= current_battery_tier)
            graphics_fill_circle(ctx, GPoint(x,y), 3);
        else
            graphics_draw_circle(ctx, GPoint(x,y), 3);
    }
}

static void battery_handler(BatteryChargeState charge_state)
{
    /*
     * 1st tier - 0%-20%
     * 2nd tier - 21%-40%
     * 3rd tier - 41%-60%
     * 4th tier - 61%-80%
     * 5th tier - 81%-100%
     */
    int new_battery_tier = charge_state.charge_percent / 20;

    if (charge_state.charge_percent % 20 == 0)
        new_battery_tier -= 1;

    if (new_battery_tier == current_battery_tier)
        return; /* No need to update the screen if there's no change */

    current_battery_tier = new_battery_tier;

    /* Update battery meter */
    layer_mark_dirty(battery_layer);
}


/* Vibrate and display "Disconnected" when Bluetooth connection is lost */
static void connection_handler(bool connected)
{
    static TextLayer *connection_layer = NULL;

    if (!connected) {
        Layer *root_layer = window_get_root_layer(main_window);
        GRect bounds = layer_get_bounds(root_layer);

        /* Create connection layer */
        connection_layer = text_layer_create(GRect(0, PBL_IF_ROUND_ELSE(15, 9), bounds.size.w, 50));
        text_layer_set_background_color(connection_layer, GColorClear);
        text_layer_set_text_color(connection_layer, foreground_color);
        text_layer_set_text(connection_layer, "Disconnected");
        text_layer_set_font(connection_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
        text_layer_set_text_alignment(connection_layer, GTextAlignmentCenter);

        layer_add_child(root_layer, text_layer_get_layer(connection_layer));

        vibes_long_pulse();

    } else if (connection_layer) {
        text_layer_destroy(connection_layer);
    }
}

static void update_color(GColor fg, GColor bg)
{
    foreground_color = fg;
    background_color = bg;

    persist_write_data(PS_FG_COLOR_KEY, &foreground_color, sizeof(GColor));
    persist_write_data(PS_BG_COLOR_KEY, &background_color, sizeof(GColor));

    window_set_background_color(main_window, background_color);
    text_layer_set_text_color(date_layer, foreground_color);
    text_layer_set_text_color(time_layer, foreground_color);

    layer_mark_dirty(text_layer_get_layer(date_layer));
    layer_mark_dirty(text_layer_get_layer(time_layer));
    layer_mark_dirty(battery_layer);
}

static void load_colors(void)
{
    foreground_color = PBL_IF_ROUND_ELSE(GColorBlack, GColorWhite);
    background_color = PBL_IF_ROUND_ELSE(GColorWhite, GColorBlack);

    if (persist_exists(PS_FG_COLOR_KEY))
        persist_read_data(PS_FG_COLOR_KEY, &foreground_color, sizeof(GColor));

    if (persist_exists(PS_BG_COLOR_KEY))
        persist_read_data(PS_BG_COLOR_KEY, &background_color, sizeof(GColor));
}

static void inbox_received_callback(DictionaryIterator *iter, void *context)
{
    GColor fg = foreground_color;
    GColor bg = background_color;

    Tuple *foreground_color_tuple = dict_find(iter, MESSAGE_KEY_foreground_color);
    Tuple *background_color_tuple = dict_find(iter, MESSAGE_KEY_background_color);

    if (foreground_color_tuple)
        fg = GColorFromHEX(foreground_color_tuple->value->int32);

    if (background_color_tuple)
        bg = GColorFromHEX(background_color_tuple->value->int32);

    update_color(fg, bg);
}

void comm_init(void)
{
    APP_LOG(APP_LOG_LEVEL_INFO, "%s: Entered", __func__);
    app_message_open(INBOX_SIZE, OUTBOX_SIZE);

    app_message_register_inbox_received(inbox_received_callback);
}

static void main_window_load(Window *window)
{
    Layer *root_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(root_layer);

    window_set_background_color(window, background_color);

    /* Create time TextLayer */
    time_layer = text_layer_create(GRect(0, PBL_IF_ROUND_ELSE(65, 59), bounds.size.w, 50));
    text_layer_set_background_color(time_layer, GColorClear);
    text_layer_set_text_color(time_layer, foreground_color);
    text_layer_set_text(time_layer, "00:00");
    text_layer_set_font(time_layer, fonts_get_system_font(FONT_KEY_LECO_42_NUMBERS));
    text_layer_set_text_alignment(time_layer, GTextAlignmentCenter);
    layer_add_child(root_layer, text_layer_get_layer(time_layer));

    /* Create date TextLayer */
    date_layer = text_layer_create(GRect(0, PBL_IF_ROUND_ELSE(41, 35), bounds.size.w, 30));
    text_layer_set_background_color(date_layer, GColorClear);
    text_layer_set_text_color(date_layer, foreground_color);
    text_layer_set_text(date_layer, "Mon, Jan 01");
    text_layer_set_font(date_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
    text_layer_set_text_alignment(date_layer, GTextAlignmentCenter);
    layer_add_child(root_layer, text_layer_get_layer(date_layer));

    /* Create battery layer */
    battery_layer = layer_create(GRect(0, PBL_IF_ROUND_ELSE(118, 112), bounds.size.w, 10));
    layer_set_update_proc(battery_layer, update_battery);
    layer_add_child(root_layer, battery_layer);
}

static void main_window_unload(Window *window)
{
    /* Destroy text layers */
    text_layer_destroy(time_layer);
    text_layer_destroy(date_layer);

    /* Destroy battery layer */
    layer_destroy(battery_layer);
}

static void deinit()
{
    window_destroy(main_window);
    tick_timer_service_unsubscribe();
    battery_state_service_unsubscribe();
    connection_service_unsubscribe();
}

static void init()
{
    load_colors();
    comm_init();

    main_window = window_create();

    window_set_window_handlers(main_window, (WindowHandlers) {
            .load = main_window_load,
            .unload = main_window_unload
        });

    window_stack_push(main_window, true);

    // Get initial time
    time_t tmp_time = time(NULL);
    struct tm *time_tick = localtime(&tmp_time);
    time_tick_handler(time_tick, MINUTE_UNIT);

    // Register for time ticks
    tick_timer_service_subscribe(MINUTE_UNIT, time_tick_handler);

    // Get initial battery
    battery_handler(battery_state_service_peek());

    // Register for battery level updates
    battery_state_service_subscribe(battery_handler);

    // Register for connection status updates
    connection_service_subscribe((ConnectionHandlers) {
            .pebble_app_connection_handler = connection_handler,
            .pebblekit_connection_handler  = NULL,
        });
}

int main()
{
    init();
    app_event_loop();
    deinit();
}
